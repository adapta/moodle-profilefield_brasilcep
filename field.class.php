<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Class profile_field_brasilcep
 *
 * @copyright  2023 Daniel Neis Araujo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class profile_field_brasilcep extends profile_field_base {

    /**
     * Add fields for editing a brasilcep profile field.
     * @param moodleform $mform
     */
    public function edit_field_add($mform) {
        global $PAGE;

        $fieldname = $this->inputname;

        $data = json_decode($this->data);

        $mform->addElement('text', $fieldname, get_string('cep', 'profilefield_brasilcep'));
        $mform->setType($fieldname, PARAM_TEXT);

        $field = $fieldname . '_logradouro';
        $mform->addElement('text', $field , get_string('logradouro', 'profilefield_brasilcep'), ['disabled' => 'disabled']);
        $mform->setType($field , PARAM_TEXT);

        $field = $fieldname . '_numero';
        $disabled = [];
        if (empty($data->cep)) {
            $disabled = ['disabled' => 'disabled'];
        }
        $mform->addElement('text', $field , get_string('numero', 'profilefield_brasilcep'), $disabled);
        $mform->setType($field , PARAM_TEXT);

        $field = $fieldname . '_complemento';
        $mform->addElement('text', $field , get_string('complemento', 'profilefield_brasilcep'), $disabled);
        $mform->setType($field , PARAM_TEXT);

        $field = $fieldname . '_bairro';
        $mform->addElement('text', $field , get_string('bairro', 'profilefield_brasilcep'), ['disabled' => 'disabled']);
        $mform->setType($field , PARAM_TEXT);

        $field = $fieldname . '_localidade';
        $mform->addElement('text', $field , get_string('localidade', 'profilefield_brasilcep'), ['disabled' => 'disabled']);
        $mform->setType($field , PARAM_TEXT);

        $field = $fieldname . '_uf';
        $mform->addElement('text', $field , get_string('uf', 'profilefield_brasilcep'), ['disabled' => 'disabled']);
        $mform->setType($field , PARAM_TEXT);

        $PAGE->requires->js_call_amd('profilefield_brasilcep/field', 'init', [$fieldname]);
    }

    /**
     * Return the field type and null properties.
     * This will be used for validating the data submitted by a user.
     *
     * @return array the param type and null property
     * @since Moodle 3.2
     */
    public function get_field_properties() {
        return array(PARAM_TEXT, NULL_NOT_ALLOWED);
    }

    /**
     * Saves the data coming from form
     *
     * @param stdClass $data data coming from the form
     * @param stdClass $datarecord The object that will be used to save the record
     */
    public function edit_save_data_preprocess($data, $datarecord) {
        $url = 'http://viacep.com.br/ws';
        $curl = new \curl();
        $data = preg_replace('~\D~', '', $data);
        $res = $curl->get($url . '/' . $data . '/json');
        if ($curl->get_info()['http_code'] == 200)  {
            $obj = json_decode($res);
            $obj->numero = optional_param($this->inputname . '_numero', '', PARAM_TEXT);
            $obj->complemento = optional_param($this->inputname . '_complemento', '', PARAM_TEXT);
            if (empty($obj->logradouro)) {
                $obj->logradouro = optional_param($this->inputname . '_logradouro', '', PARAM_TEXT);
            }
            if (empty($obj->bairro)) {
                $obj->bairro = optional_param($this->inputname . '_bairro', '', PARAM_TEXT);
            }
            if (empty($obj->localidade)) {
                $obj->localidade = optional_param($this->inputname . '_localidade', '', PARAM_TEXT);
            }
            if (empty($obj->uf)) {
                $obj->uf = optional_param($this->inputname . '_uf', '', PARAM_TEXT);
            }
            return json_encode($obj, JSON_UNESCAPED_UNICODE);
        } else {
            return '';
        }
    }

    /**
     * Sets the default data for the field in the form object
     * @param  moodleform $mform instance of the moodleform class
     */
    public function edit_field_set_default($mform) {
        $mform->setDefault($this->inputname, '');
    }

    /**
     * When passing the user object to the form class for the edit profile page
     * we should load the key for the saved data
     *
     * Overwrites the base class method.
     *
     * @param stdClass $user User object.
     */
    public function edit_load_user_data($user) {
        if (!empty($this->data)) {
            $data = json_decode($this->data);
            if ($data) {
                $user->{$this->inputname} = $data->cep;
                $user->{$this->inputname . '_logradouro'} = $data->logradouro;
                $user->{$this->inputname . '_numero'} = $data->numero;
                $user->{$this->inputname . '_complemento'} = $data->complemento;
                $user->{$this->inputname . '_bairro'} = $data->bairro;
                $user->{$this->inputname . '_localidade'} = $data->localidade;
                $user->{$this->inputname . '_uf'} = $data->uf;
            }
        }
    }

    /**
     * Display the data for this field
     * @return string
     */
    public function display_data() {
        $data = json_decode($this->data);
        $display = $data->logradouro . ', ' . $data->numero . (empty($data->complemento) ?  '' : ', ' . $data->complemento) .
            ', ' . $data->bairro .  ', CEP: ' . $data->cep . ', ' . $data->localidade . ' - ' . $data->uf;
        return $display;
    }

    public function edit_validate_field($data) {
        $errors = [];
        return $errors;
    }

    /**
     * Sets the required flag for the field in the form object
     *
     * @param moodleform $mform instance of the moodleform class
     */
    public function edit_field_set_required($mform) {
        global $USER;
        if ($this->is_required()) {
            $mform->addRule($this->inputname, get_string('required'), 'required', null, 'client');
        }
    }
}
