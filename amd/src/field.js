// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handle opening a dialogue to configure condition data.
 *
 * @module     profilefield_brasilcep/field
 * @copyright  2023 Daniel Neis Araujo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define(
    ['core/ajax'],
    function(ajax) {

        var cep = '';
        var inputname = '';

        var get_info = function() {
            if ((cep.value != undefined) && cep.value != "") {
                ajax.call([{
                    methodname: 'profilefield_brasilcep_get_info',
                    args: {cep: cep.value.replace(/\D/g, "")},
                    done: function(info) {

                        let logradouro = document.getElementById('id_' + inputname + '_logradouro');
                        if (info.logradouro == '') {
                            logradouro.value = '';
                            logradouro.disabled = false;
                        } else {
                            logradouro.value = info.logradouro;
                            logradouro.disabled = true;
                        }

                        document.getElementById('id_' + inputname + '_numero').disabled = false;

                        let complemento = document.getElementById('id_' + inputname + '_complemento');
                        if (complemento.value == "") {
                            complemento.value = info.complemento;
                        }
                        document.getElementById('id_' + inputname + '_complemento').disabled = false;

                        let bairro = document.getElementById('id_' + inputname + '_bairro');
                        if (info.bairro == "") {
                            bairro.value = "";
                            bairro.disabled = false;
                        } else {
                            bairro.value = info.bairro;
                            bairro.disabled = true;
                        }

                        let localidade = document.getElementById('id_' + inputname + '_localidade');
                        if (info.localidade == "") {
                            localidade.value = "";
                            localidade.disabled = false;
                        } else {
                            localidade.value = info.localidade;
                            localidade.disabled = true;
                        }

                        let uf = document.getElementById('id_' + inputname + '_uf');
                        if (info.uf == "") {
                            uf.value = "";
                            uf.disabled = false;
                        } else {
                            uf.value = info.uf;
                            uf.disabled = true;
                        }

                        return true;
                    }
                }]);
            }
        };

        return {
            init: function(inputnameparam) {
                inputname = inputnameparam;
                cep = document.getElementById('id_' + inputname);
                cep.addEventListener('focusout', get_info);
                cep.addEventListener('change', function() {
                    let n = cep.value.replace(/(\d{5})(\d{3})/,"$1-$2");
                    this.value = n;
                });
            }
        };
    }
);
