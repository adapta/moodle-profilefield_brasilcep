<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace profilefield_brasilcep\external;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/filelib.php');

/**
 * Provides the profilefield_brasilcep_get_municipios external function.
 *
 * @package     profilefield_brasilcep
 * @category    external
 * @copyright   2023 Daniel Neis Araujo
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class get_info extends \external_api {

    /**
     * Describes the external function parameters.
     *
     * @return external_function_parameters
     */
    public static function execute_parameters(): \external_function_parameters {

        return new \external_function_parameters([
            'cep' => new \external_value(PARAM_TEXT, 'CEP to get info', VALUE_REQUIRED),
        ]);
    }

    /**
     * Finds info for given CEP.
     *
     * @param string $cep The search request.
     * @return \stdclass 
     */
    public static function execute(string $cep): \stdclass {
        global $DB, $CFG;

        $params = \external_api::validate_parameters(self::execute_parameters(), [
            'cep' => $cep,
        ]);
        $cep = $params['cep'];

        $url = 'http://viacep.com.br/ws';
        $curl = new \curl();
        $cep = preg_replace('~\D~', '', $cep);
        $res = $curl->get($url . '/' . $cep . '/json');
        if ($res = json_decode($res)) {
            return $res;
        } else {
            return new \stdClass();
        }

    }

    /**
     * Describes the external function result value.
     *
     * @return external_description
     */
    public static function execute_returns(): \external_description {
        return new \external_single_structure([
            'cep'  => new \external_value(PARAM_TEXT, 'CEP'),
            'logradouro' => new \external_value(PARAM_TEXT, 'Logradouro of the CEP.'),
            'complemento' => new \external_value(PARAM_TEXT, 'Complemento of the CEP.'),
            'bairro' => new \external_value(PARAM_TEXT, 'Bairro of the CEP.'),
            'localidade' => new \external_value(PARAM_TEXT, 'Localidade of the CEP.'),
            'uf' => new \external_value(PARAM_TEXT, 'UF of the CEP.'),
            'ibge' => new \external_value(PARAM_TEXT, 'IBGE of the CEP.'),
            'gia' => new \external_value(PARAM_TEXT, 'GIA of the CEP.'),
            'ddd' => new \external_value(PARAM_TEXT, 'DDD of the CEP.'),
            'siafi' => new \external_value(PARAM_TEXT, 'SIAFI of the CEP.'),
        ]);
    }
}
