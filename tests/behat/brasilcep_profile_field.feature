@profilefield @profilefield_brasilcep
Feature: Brasil CEP profile fields can not have a duplicate shortname.
  In order edit Brasil CEP profile fields properly
  As an admin
  I should not be able to create duplicate shortnames for Brasil CEP profile fields.

  @javascript
  Scenario: Verify you can edit Brasil CEP profile fields.
    Given I log in as "admin"
    When I navigate to "Users > Accounts > User profile fields" in site administration
    And I click on "Create a new profile field" "link"
    And I click on "Brasil CEP" "link"
    And I set the following fields to these values:
      | Short name | anyrandomone |
      | Name       | Field name Brasil CEP |
    And I click on "Save changes" "button"

    And I click on "Create a new profile field" "link"
    And I click on "Brasil CEP" "link"
    And I set the following fields to these values:
      | Short name | anyrandomone |
      | Name       | Field name Brasil CEP |
    And I click on "Save changes" "button"
    Then I should see "This short name is already in use"
