<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Plugin upgrade steps are defined here.
 *
 * @package     profilefield_brasilcep
 * @category    upgrade
 * @copyright   2023 Daniel Neis Araujo
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Execute profilefield_brasilcep upgrade from the given old version.
 *
 * @param int $oldversion
 * @return bool
 */
function xmldb_profilefield_brasilcep_upgrade($oldversion) {
    global $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2023050300) {

        $fieldssql = "SELECT id FROM {user_info_field} WHERE datatype = ?";
        $fields = $DB->get_records_sql($fieldssql, ['brasilcep']);
        foreach ($fields as $f) {
            $sql = "SELECT id, data
                      FROM {user_info_data} ud
                      WHERE ud.fieldid = ?";
            $data = $DB->get_records_sql($sql, [$f->id]);
            foreach ($data as $d) {
                $record = new stdclass();
                $record->id = $d->id;
                $record->data = json_encode(json_decode($d->data), JSON_UNESCAPED_UNICODE);
                $DB->update_record('user_info_data', $record);
            }
        }

        upgrade_plugin_savepoint(true, 2023050300, 'profilefield', 'brasilcep');
    }

    return true;
}
